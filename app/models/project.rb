class Project < ActiveRecord::Base
  validates :name, presence: true, length: { minimum: 5 }
  validates :description, presence: true
  validates :workspace_id, presence: true, numericality: true

  belongs_to :workspace
end

class Workspace < ActiveRecord::Base
  validates :title, presence: true, length: { minimum: 5 }
  validates :description, presence: true

  has_many :projects
end

class WorkspacesController < ApplicationController
  before_action :fetch_workspace, only: [:show, :update, :destroy]
  def index
    @workspaces = Workspace.all
    render json: @workspaces, status: 200
  end

  def create
    @workspace = Workspace.new(workspace_params)
    if @workspace.save
      render json: @workspace, status: 201, location: @workspace
    else
      render json: @workspace.errors.to_json, status: 422
    end
  end

  def show
    render json: @workspace, status: 200
  end

  def update
    if @workspace.update(workspace_params)
      render json: @workspace, status: 200, location: @workspace
    else
      render json: @workspace.errors.to_json, status: 422
    end
  end

  def destroy
    @workspace.destroy
    head 204
  end

  private

  def fetch_workspace
    @workspace = Workspace.find(params[:id])
  end

  def workspace_params
    params.require(:workspace).permit(:title, :description)
  end
end

require 'test_helper'

class DeletingWorkspacesTest < ActionDispatch::IntegrationTest
  setup { @workspace = Workspace.create!(title: "Test title", description: "Test description") }

  test 'deleting workspace' do
    delete "/workspaces/#{@workspace.id}"
    assert_equal 204, response.status
  end
end
require 'test_helper'

class UpdatingWorkspacesTest < ActionDispatch::IntegrationTest
  setup { @workspace = Workspace.create!(title: "Test title", description: "Test description") }

  test 'successful update' do
    patch "/workspaces/#{@workspace.id}", { workspace: { title: 'Updated test title' } }.to_json,
      { 'Accept' => Mime::JSON, 'Content-Type' => Mime::JSON.to_s }
    assert_equal 200, response.status
    assert_equal 'Updated test title', @workspace.reload.title
  end

  test 'unsuccessful update on short title' do
    patch "/workspaces/#{@workspace.id}", { workspace: { title: 'WtF?' } }.to_json,
      { 'Accept' => Mime::JSON, 'Content-Type' => Mime::JSON.to_s }
    assert_equal 422, response.status
  end
end
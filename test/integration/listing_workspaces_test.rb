require 'test_helper'

class ListingWorkspacesTest < ActionDispatch::IntegrationTest
  test 'returns list of all workspaces' do
    get '/workspaces'
    assert_equal 200, response.status
    refute_empty response.body
  end

  test 'returns workspaces by id' do
    w = Workspace.create!(title: "Test title", description: "Test description")
    get "/workspaces/#{w.id}"
    assert_equal 200, response.status

    workspace_responce = json(response.body)
    assert_equal w.title, workspace_responce[:title]
  end

  test 'returns workspaces in JSON' do
    get '/workspaces', {}, { 'Accept' => Mime::JSON }
    assert_equal 200, response.status
    assert_equal Mime::JSON, response.content_type
  end
end
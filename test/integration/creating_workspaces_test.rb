require 'test_helper'

class CreatingWorkspacesTest < ActionDispatch::IntegrationTest
  test 'creates workspace' do
    post '/workspaces', { workspace:
                          { title: 'Some test title',
                            description: 'Some test description' }}.to_json,
                        { 'Accept' => Mime::JSON, 'Content-Type' => Mime::JSON.to_s }
    assert_equal 201, response.status
    assert_equal Mime::JSON, response.content_type

    workspace = json(response.body)
    assert_equal workspace_url(workspace[:id]), response.location
  end

  test 'does not create workspace with title nil' do
    post '/workspaces', { workspace:
                          { title: nil,
                            description: 'Some test description' }}.to_json,
                        { 'Accept' => Mime::JSON, 'Content-Type' => Mime::JSON.to_s }
    assert_equal 422, response.status
    assert_equal Mime::JSON, response.content_type
  end
end
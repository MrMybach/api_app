require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  test 'can save with all attributes' do
    p = projects(:one)
    assert_equal 'sit amet consectetur adipisicing elit Laboriosam necessitatibus', p.description
  end

  test 'cannot save without description' do
    p = projects(:one)
    p.description = nil
    p.save
    assert !p.new_record?, 'cannot be saved without description'
  end

  test 'cannot save without workspace_id' do
    p = projects(:one)
    p.workspace_id = nil
    p.save
    assert !p.new_record?, 'cannot be saved without workspace_id'
  end
end

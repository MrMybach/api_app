require 'test_helper'

class WorkspaceTest < ActiveSupport::TestCase
  test 'can save with all attributes' do
    w = workspaces(:one)
    assert_equal 'Lorem ipsum dolor sit amet2', w.description
  end

  test 'cannot save without description' do
    w = workspaces(:one)
    w.description = nil
    w.save
    assert !w.new_record?, 'cannot be saved without description'
  end

  test 'should respond with projects' do
    w = workspaces(:one)
    assert_respond_to w, :projects
  end

  test 'should contain only projects that belongd to workspace' do
    w = workspaces(:two)
    assert w.projects.all? { |p| p.workspace == w }
  end
end
